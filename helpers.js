import { config } from "https://deno.land/x/dotenv/mod.ts";
import { join } from "https://deno.land/std/path/mod.ts";
import { expandGlob } from "https://deno.land/std/fs/mod.ts";

import { DATA_DIR, FEATURE_FLAGS_DICTIONARY_FILE } from "./constants.js";

const { API_TOKEN, API_ROOT } = config();

const objectToQueryString = (params) =>
  params
    ? Object.entries(params).reduce(
      (queryString, [key, value]) => queryString += `&${key}=${value}`,
      "?",
    )
    : "";

export const buildRequest = (endpoint, params = null) =>
  new Request(API_ROOT + endpoint + objectToQueryString(params), {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${API_TOKEN}`,
    },
  });

export const getFeatureFlagsDictionary = async () => {
  const content = await Deno.readTextFile(
    join(DATA_DIR, FEATURE_FLAGS_DICTIONARY_FILE),
  );
  return JSON.parse(content);
};

export const getFeatureFlagFilesFromBlob = async (blob) => {
  const tmpDir = Deno.env.get("TMPDIR");
  const buffer = await blob.arrayBuffer();
  const unit8arr = new Deno.Buffer(buffer).bytes();
  const zipFilePath = join(tmpDir, "feature_flags.zip");
  await Deno.writeFile(zipFilePath, unit8arr, {
    create: true,
  });
  const unzipPath = join(tmpDir, "feature_flags");
  const unzipCmd = Deno.run({
    cmd: ["unzip", zipFilePath, "-d", unzipPath],
    stdout: "piped",
    stderr: "piped",
  });
  return expandGlob(join(unzipPath, "**/*.yml"));
};
