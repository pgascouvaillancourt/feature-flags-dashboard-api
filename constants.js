// Local data storage
export const DATA_DIR = "data";
export const LATEST_COMMIT_FILE = "latest_commit";
export const FEATURE_FLAGS_DICTIONARY_FILE = "feature_flags.json";

// Request parameters
export const ARCHIVE_URL =
  "https://gitlab.com/gitlab-org/gitlab/-/archive/master/gitlab-master.zip?path=config/feature_flags/development";
export const PROJECT_PATH = encodeURIComponent("gitlab-org/gitlab");
export const FEATURE_FLAGS_DIR = encodeURIComponent(
  "config/feature_flags/development",
);

// Feature flags dictionary
export const NO_GROUP_KEY = "no_group";
