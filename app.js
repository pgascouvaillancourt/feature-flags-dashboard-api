import { Application } from "https://deno.land/x/oak/mod.ts";
import { oakCors } from "https://deno.land/x/cors/mod.ts";
import * as log from "https://deno.land/std/log/mod.ts";
import { router } from "./router.js";

const app = new Application();
app.use(oakCors());
app.use(router.routes());
app.use(router.allowedMethods());

const host = "127.0.0.1:8000";

log.info(`Starting server at http://${host}`);

await app.listen(host);
