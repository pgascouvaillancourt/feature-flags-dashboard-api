import { helpers, Router } from "https://deno.land/x/oak/mod.ts";
import { Service } from "./service.js";

export const router = new Router();
router
  /**
   * Returns the list of groups cached
   * in the local dictionary.
   */
  .get("/groups", async (ctx) => {
    ctx.response.body = await Service.getCachedGroups();
  })
  /**
   * Get cached feature flags data for the given groups.
   */
  .get("/flags_for_groups", async (ctx) => {
    const params = helpers.getQuery(ctx);
    const groups = params.groups ? params.groups.split(",") : [];
    ctx.response.body = await Service.getFlagsForGroups(groups);
  });
