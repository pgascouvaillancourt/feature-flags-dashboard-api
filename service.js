import { join } from "https://deno.land/std/path/mod.ts";
import { decode } from "https://deno.land/std/encoding/base64.ts";
import { parse } from "https://deno.land/std/encoding/yaml.ts";
import * as log from "https://deno.land/std/log/mod.ts";

import {
  ARCHIVE_URL,
  DATA_DIR,
  FEATURE_FLAGS_DICTIONARY_FILE,
  FEATURE_FLAGS_DIR,
  LATEST_COMMIT_FILE,
  NO_GROUP_KEY,
  PROJECT_PATH,
} from "./constants.js";
import {
  buildRequest,
  getFeatureFlagFilesFromBlob,
  getFeatureFlagsDictionary,
} from "./helpers.js";

export class Service {
  static async hasChanges() {
    // Read latest commit ID from cache
    let cachedLatestCommit;
    try {
      cachedLatestCommit = await Deno.readTextFile(
        join(DATA_DIR, LATEST_COMMIT_FILE),
      );
    } catch {
      cachedLatestCommit = "";
    }

    // Fetch latest commit from repo
    const request = buildRequest(
      `/projects/${PROJECT_PATH}/repository/commits`,
      {
        path: FEATURE_FLAGS_DIR,
      },
    );
    const data = await fetch(request);
    const json = await data.json();
    const [latestCommit] = json;

    if (latestCommit.id !== cachedLatestCommit) {
      Deno.writeTextFile(join(DATA_DIR, LATEST_COMMIT_FILE), latestCommit.id);
      return true;
    }
    return false;
  }

  static async updateCache() {
    const hasChanges = await Service.hasChanges();
    if (!hasChanges) {
      return false;
    }
    // Retrieve all feature flag YAML files
    const request = new Request(ARCHIVE_URL);
    const data = await fetch(request);
    const blob = await data.blob();
    const files = await getFeatureFlagFilesFromBlob(blob);

    // Build a dictionary of feature flags
    const dictionary = {};
    for await (const file of files) {
      try {
        const yamlRaw = await Deno.readTextFile(file.path);
        const yaml = parse(yamlRaw);
        const {
          group,
          name,
          introduced_by_url,
          rollout_issue_url,
          default_enabled,
        } = yaml;
        const key = group || NO_GROUP_KEY;
        dictionary[key] = dictionary[key] || [];
        dictionary[key].push({
          name,
          path: file.path,
          introduced_by_url,
          rollout_issue_url,
          default_enabled,
        });
      } catch (e) {
        log.error(`Could not parse ${file.path}`);
        log.error(e);
      }
    }
    await Deno.writeTextFile(
      join(DATA_DIR, FEATURE_FLAGS_DICTIONARY_FILE),
      JSON.stringify(dictionary),
    );
    return true;
  }

  static async getCachedGroups() {
    await Service.updateCache();
    const dictionary = await getFeatureFlagsDictionary();
    return Object.keys(dictionary);
  }

  static async getFlagsForGroups(groups = []) {
    await Service.updateCache();
    const res = {};
    const keys = new Set(groups);
    if (keys.size === 0) {
      return res;
    }
    const dictionary = await getFeatureFlagsDictionary();
    groups.forEach((group) => {
      res[group] = dictionary[group];
    });
    return res;
  }
}
